package beans;

import java.io.Serializable;

public class Attempt implements Serializable
{
    private double x, y, r;
    private String curTime;
    private double execTime;
    private boolean result;

    public Attempt(double x, double y, double r, String curTime, double execTime, boolean result)
    {
        this.x = x;
        this.y = y;
        this.r = r;
        this.curTime = curTime;
        this.execTime = execTime;
        this.result = result;
    }

    public double getX()
    {
        return x;
    }

    public void setX(double x)
    {
        this.x = x;
    }

    public double getY()
    {
        return y;
    }

    public void setY(double y)
    {
        this.y = y;
    }

    public double getR()
    {
        return r;
    }

    public void setR(double r)
    {
        this.r = r;
    }

    public String getCurTime()
    {
        return curTime;
    }

    public void setCurTime(String curTime)
    {
        this.curTime = curTime;
    }

    public double getExecTime()
    {
        return execTime;
    }

    public void setExecTime(long execTime)
    {
        this.execTime = execTime;
    }

    public boolean isResult()
    {
        return result;
    }

    public void setResult(boolean result)
    {
        this.result = result;
    }
}
