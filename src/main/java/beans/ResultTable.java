package beans;

import java.io.Serializable;
import java.util.LinkedList;

public class ResultTable implements Serializable
{
    private LinkedList<Attempt> results;

    public ResultTable()
    {
        results = new LinkedList<>();
    }

    public LinkedList<Attempt> getResults()
    {
        return results;
    }

    public void setResults(LinkedList<Attempt> results)
    {
        this.results = results;
    }

    public void add(Attempt attempt)
    {
        results.addFirst(attempt);
    }

    public void addAll(LinkedList<Attempt> results)
    {
        this.results.addAll(results);
    }

    public boolean isEmpty()
    {
        return results.isEmpty();
    }

    public Attempt getLast()
    {
        return results.getLast();
    }

}
