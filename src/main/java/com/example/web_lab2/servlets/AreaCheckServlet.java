package com.example.web_lab2.servlets;

import beans.Attempt;
import beans.ResultTable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class AreaCheckServlet extends HttpServlet
{
    boolean checkPoint(double x, double y, double r)
    {
        if (!(r == 1 || r == 2 || r == 3 || r == 4 || r == 5))
            throw new IllegalArgumentException();

        return
                x >= 0 && y >= 0 && (x * x + y * y) * 4 <= r * r ||
                        x <= 0 && y <= 0 && 2 * x >= -r && y >= -r ||
                        x >= 0 && y <= 0 && y >= x - r;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException
    {
        ResultTable resultTable = (ResultTable) req.getAttribute("resultTable");

        String curTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"));

        long startTime = System.nanoTime();

        double x, y, r;
        try {
            x = Double.parseDouble(req.getParameter("x_input"));
            y = Double.parseDouble(req.getParameter("y_input"));
            r = Double.parseDouble(req.getParameter("r_input"));
        } catch (NumberFormatException e) {
            resp.sendError(400, "Invalid input");
            return;
        }

        boolean curResult = false;

        try {
            curResult = checkPoint(x, y, r);
        } catch (IllegalArgumentException e) {
            resp.sendError(400, "Invalid input");
        }

        double execTime = System.nanoTime() - startTime;

        resultTable.add(new Attempt(x, y, r, curTime, execTime / 10e6, curResult));

        req.getRequestDispatcher("/index.jsp").forward(req, resp);

    }

}
