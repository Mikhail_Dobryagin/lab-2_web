package com.example.web_lab2.servlets;

import beans.ResultTable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ControllerServlet extends HttpServlet
{
    private final ResultTable resultTable = new ResultTable();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {

        req.setAttribute("resultTable", resultTable);

        String[] x = req.getParameterValues("x_input");
        String y = req.getParameter("y_input");
        String r = req.getParameter("r_input");

        if (x != null && y != null && r != null)
            req.getRequestDispatcher("/AreaCheckServlet").forward(req, resp);
        else
            req.getRequestDispatcher("/index.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        req.getRequestDispatcher("/index.jsp").forward(req, resp);
    }
}
