var r_old;
var y_text_Old = '';

$(document).ready(function () {

    $('#y_text').on('input', checkYInput);

    $('#input_form').on('submit', submitForm);

    $('#graph').on('click', clickGraph);

    $('.x_input_div input[type=button]').on('click', function () {

        $('#x_input').val(this.value);

        $('.x_input_div input[type=button]').each(function () {
            this.style.color = "white";
        });

        this.style.color = "black";
    });

    let pushedButtonRVal = Number($('#result_table_body tbody tr:first-child td:nth-child(3)').text()).toFixed(0);

    $('#input_form input[name="r_input"][value="' + pushedButtonRVal + '"]').click();

    $('#input_form input[name="r_input"]').on('click', function () {
        redrawPoints();
        r_old = $('#input_form input[name="r_input"]:checked').val();
    });

    r_old = $('#input_form input[name="r_input"]:checked').val();
});

function submitForm() {
    flag = true;

    if ($('#input_form input[name="r_input"]:checked').val() === undefined) {
        wrongInputAnimation($('#r_label'));
        flag = false;
    }


    if ($('#input_form input[name="x_input"]').val() === '') {
        wrongInputAnimation($('#x_label'));
        flag = false;
    }

    if ($('#input_form input[name="y_input"]').val() === '') {
        wrongInputAnimation($('#y_label'));
        flag = false;
    }

    if (flag)
        $('#submit_button_real').trigger('click');

}

function checkYChange(y_text) {
    return y_text !== '-' && Number(y_text) <= 5 && Number(y_text) >= -3 && !isNaN(Number(y_text));
}

function checkYInput() {
    let y_text = $('#y_text').val();

    if (!checkYChange(y_text)) {
        $('#y_text').val(y_text_Old);
        $('#y_input').val(y_text_Old);
        return;
    }

    y_text_Old = y_text;
    $('#y_input').val(y_text);
}

function checkY(y_text) {
    return !(y_text === '' || Number(y_text) >= 5 || Number(y_text) <= -3 || isNaN(Number(y_text)));
}

function checkX() {

}

function clickGraph(event) {
    let r = $('input[name=r_input]:checked').val();

    if (r === undefined) {
        wrongInputAnimation($('#r_label'));
        return;
    }

    let xPage = event.pageX - $('#graph').offset().left;
    let x = ((xPage - 110) * r / (176 - 110)).toFixed(2);

    let yPage = event.pageY - $('#graph').offset().top;
    let y = ((110 - yPage) * r / (176 - 110)).toFixed(2);

    drawPoint(xPage, yPage);

    $('#x_input').val(x);
    $('#y_input').val(y);

    $('#submit_button').trigger('click');
}

function drawPoint(xPage, yPage) {

    $('#graph circle:last-child').attr("cx", xPage);
    $('#graph circle:last-child').attr("cy", yPage);
    $('#graph circle:last-child').attr("r", 2);

}

async function wrongInputAnimation(element) {
    element.animate({
        opacity: 0
    }, 1000);

    setTimeout(function () {
        element.css("color", "red");

        element.animate({
            opacity: 1
        }, 1000);

        setTimeout(function () {

            element.animate({
                opacity: 0
            }, 5000);

            setTimeout(function () {
                element.css("color", "black");

                element.animate({
                    opacity: 1
                }, 1000);
            }, 5000);

        }, 1000);
    }, 1000);
}

function resetForm() {
    $('.x_input_div input[type=button]').each(function () {
        this.style.color = "white";
    });
}

function redrawPoints() {
    if (r_old === undefined)
        return;

    $('.point').each(function () {
        //alert(this.getAttribute("cx"));
        if (this.getAttribute("cx") === undefined) {
            alert("Это последний элемент");
            return;
        }

        let xPage = this.getAttribute("cx");
        let x = (xPage - 110) * r_old / (176 - 110);

        let yPage = this.getAttribute("cy");
        let y = (110 - yPage) * r_old / (176 - 110);


        let r = $('#input_form input[name="r_input"]:checked').val();

        xPage = x / r * (176 - 110) + 110;
        yPage = 110 - y / r * (176 - 110);

        //alert(this.getAttribute("cx") + " " + this.getAttribute("cy"));
        this.setAttribute("cx", xPage);
        this.setAttribute("cy", yPage);
        //alert(this.getAttribute("cx") + " " + this.getAttribute("cy"));
    });
}