<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Labka 2</title>
    <link rel="icon" href="img/puppy.ico">

    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>


<body>
<table id="main_table" style="border: 0 solid black;">

    <tr>
        <td id="main_header" class='table_title' colspan="2">
            <span style="float: left">Добрягин Михаил Александрович (P3231)</span>
            <span style="float: right">5486</span>
        </td>
    </tr>

    <tr class="input_tr">
        <td class="card card_left" id="card_top">
            <div class="card_title table_title" style="border-radius: 20px 5px 0 0;">
                Область
            </div>

            <div class="card_space">
                <svg id="graph" width="220" height="220" class="svg-graph"
                     xmlns="http://www.w3.org/2000/svg">

                    <!-- X-Axis -->
                    <line class="axis" x1="10" y1="110" x2="210" y2="110" stroke="black"/>
                    <polygon points="210,110 200,105 200,115"/>

                    <!-- Y-Axis -->
                    <line class="axis" x1="110" y1="10" x2="110" y2="210" stroke="black"/>
                    <polygon points="110,10 105,20 115,20"/>

                    <!-- X-Axis coordinates-->
                    <line class="coordinate-line" x1="43" y1="105" x2="43" y2="115" stroke="black"/>
                    <text class="coordinate-text" x="33" y="100" font-size="14">-R</text>

                    <line class="coordinate-line" x1="76" y1="105" x2="76" y2="115" stroke="black"/>
                    <text class="coordinate-text" x="66" y="100" font-size="14">-R/2</text>

                    <line class="coordinate-line" x1="143" y1="105" x2="143" y2="115" stroke="black"/>
                    <text class="coordinate-text" x="138" y="100" font-size="14">R/2</text>

                    <line class="coordinate-line" x1="176" y1="105" x2="176" y2="115" stroke="black"/>
                    <text class="coordinate-text" x="171" y="100" font-size="14">R</text>

                    <!-- Y-Axis coordinates-->
                    <line class="coordinate-line" x1="105" y1="176" x2="115" y2="176" stroke="black"/>
                    <text class="coordinate-text" x="120" y="181" font-size="14">-R</text>

                    <line class="coordinate-line" x1="105" y1="143" x2="115" y2="143" stroke="black"/>
                    <text class="coordinate-text" x="120" y="148" font-size="14">-R/2</text>

                    <line class="coordinate-line" x1="105" y1="76" x2="115" y2="76" stroke="black"/>
                    <text class="coordinate-text" x="120" y="81" font-size="14">R/2</text>

                    <line class="coordinate-line" x1="105" y1="43" x2="115" y2="43" stroke="black"/>
                    <text class="coordinate-text" x="120" y="48" font-size="14">R</text>

                    <!-- Triangle -->
                    <polygon class="svg-figure triangle-figure" points="110,110 110,176 176,110"
                             fill="blue" fill-opacity="0.25" stroke="darkblue" stroke-opacity="0.5"/>

                    <!-- Circle -->
                    <path class="svg-figure circle-figure" d="M 143 110 A 33 33 0 0 0 110 76 L 110 110 Z"
                          fill="blue" fill-opacity="0.25" stroke="darkblue" stroke-opacity="0.5"/>

                    <!-- Rectangle -->
                    <polygon class="svg-figure rectangle-figure" points="76,110 110,110 110,176 76,176"
                             fill="blue" fill-opacity="0.25" stroke="darkblue" stroke-opacity="0.5"/>


                    <jsp:useBean id="resultTable" scope="request" class="beans.ResultTable"/>

                    <c:forEach var="attempt" items="${resultTable.results}">
                        <circle class="point" fill="#003850" fill-opacity="0.5" stroke="#003850" stroke-width="1" cx="${attempt.x*(176.-110.)/resultTable.results.peekFirst().r+110.}" cy="${110. - attempt.y*(176.- 110.)/resultTable.results.peekFirst().r}" r="2"/>
                    </c:forEach>

                    <circle fill="#003850" fill-opacity="0.5" stroke="#003850" stroke-width="1"/>
                </svg>

            </div>

        </td>

        <td rowspan="2" class="card" id="card_right">
            <div style="border-radius: 5px 20px 0 0;" class="card_title table_title top_title">
                Результаты
            </div>

            <div class="card_space">
                <table class="result_table" style="height:5%;">
                    <tr style="background: WhiteSmoke;">
                        <th class="input_data_column">X</th>
                        <th class="input_data_column">Y</th>
                        <th class="input_data_column">R</th>
                        <th class="time_column">Время запуска</th>
                        <th class="time_column">Время работы</th>
                        <th class="result_column">ВЕРДИКТ</th>
                    </tr>
                </table>

                <div style="overflow-x: auto; height: 95%;">
                    <table id="result_table_body" class="result_table">
                        <tbody>

                        <c:forEach var="attempt" items="${resultTable.results}">
                            <tr>
                                <td class="input_data_column">${attempt.x}</td>
                                <td class="input_data_column">${attempt.y}</td>
                                <td class="input_data_column">${attempt.r}</td>
                                <td class="time_column">${attempt.curTime}</td>
                                <td class="time_column">${attempt.execTime}</td>
                                <td class="result_column">
                                    <img src="img/${attempt.result ? "tick" : "cross"}.svg"/>
                                </td>


                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>

            <div style="border-radius: 0 0 20px 5px;" class="card_title table_title bottom_title">
                <br/>
            </div>
        </td>
    </tr>

    <tr>
        <td class="card card_left" id="card_down">
            <div class="card_space">
                <form id="input_form" action="${pageContext.request.contextPath}/ControllerServlet" method="POST">
                    <table id="input_table">

                        <!-- X -->
                        <tr class="input_tr">
                            <td class="input_label"><label id="x_label">X:</label></td>

                            <td class="input_values">
                                <div class="input_div x_input_div">
                                    <input id="x_button1" type="button" value="-4"/>
                                </div>

                                <div class="input_div x_input_div">
                                    <input id="x_button2" type="button" value="-3"/>
                                </div>

                                <div class="input_div x_input_div">
                                    <input id="x_button3" type="button" value="-2"/>
                                </div>

                                <div class="input_div x_input_div">
                                    <input id="x_button4" type="button" value="-1"/>
                                </div>

                                <div class="input_div x_input_div">
                                    <input id="x_button5" type="button" value="0"/>
                                </div>

                                <div class="input_div x_input_div">
                                    <input id="x_button6" type="button" value="1"/>
                                </div>

                                <div class="input_div x_input_div">
                                    <input id="x_button7" type="button" value="2"/>
                                </div>

                                <div class="input_div x_input_div">
                                    <input id="x_button8" type="button" value="3"/>
                                </div>

                                <div class="input_div x_input_div">
                                    <input id="x_button9" type="button" value="4"/>
                                </div>

                                <div style="visibility: hidden;">
                                    <input id="x_input" type="text" name="x_input"/>
                                </div>
                            </td>
                        </tr>

                        <!-- Y -->
                        <tr class="input_tr">
                            <td class="input_label">
                                <label id="y_label" for="y_text">Y:</label>
                            </td>
                            <td class="input_div">
                                <input name="y_text" id="y_text" type="text" placeholder="Значение из (-3;5)"/>
                                <input style="display: none;" name="y_input" id="y_input" type="text"/>
                            </td>

                        </tr>

                        <!-- R -->
                        <tr class="input_tr">
                            <td class="input_label"><label id="r_label">R:</label></td>

                            <td class="input_values">
                                <div class="input_div">
                                    <label for="r_radio1" class="uint_number">1</label>
                                    <input id="r_radio1" type="radio" name="r_input" value="1"/>
                                </div>

                                <div class="input_div">
                                    <label for="r_radio2" class="uint_number">2</label>
                                    <input id="r_radio2" type="radio" name="r_input" value="2"/>
                                </div>

                                <div class="input_div">
                                    <label for="r_radio3" class="uint_number">3</label>
                                    <input id="r_radio3" type="radio" name="r_input" value="3"/>
                                </div>

                                <div class="input_div">
                                    <label for="r_radio4" class="uint_number">4</label>
                                    <input id="r_radio4" type="radio" name="r_input" value="4"/>
                                </div>

                                <div class="input_div">
                                    <label for="r_radio5" class="uint_number">5</label>
                                    <input id="r_radio5" type="radio" name="r_input" value="5"/>
                                </div>

                            </td>
                        </tr>

                        <!-- Buttons -->
                        <tr>
                            <td colspan="2">
                                <div class="buttons">
                                    <input id="submit_button" class="button" type="button" onclick="submitForm()" value="Отправить">
                                    <input id="submit_button_real" type="submit" style="display: none">
                                    <input id="reset_button" class="button" type="reset" value="Сбросить" onclick="resetForm()">
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div style="border-radius: 0 0 5px 20px;" class="card_title table_title">
                Значения
            </div>

        </td>
    </tr>


</table>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="js/script.js"></script>
</body>

</html>
